const db = require('./dataBase/db')
const response = require('./support_server_files/response')
require('dotenv').config()

const getHouses = (req, res) => {
  const innMC = req.params.inn
  console.log('inn: ' + innMC)
  const sqlGetIdManagementCompany = 'SELECT id FROM ' + process.env.TABLE_FOR_MC + " WHERE inn = '" + innMC + "';"
  db.query(sqlGetIdManagementCompany, (err, results) => {
    if (err) {
      console.log(err)
      return
    }
    const idMC = results.rows[0].id
    console.log('id: ' + idMC)
    const sqlGetAllHousesMC = 'SELECT * FROM ' + process.env.TABLE_FOR_HOUSES + " WHERE management_organization_id = '" + idMC + "';"
    console.log(sqlGetAllHousesMC)
    db.query(sqlGetAllHousesMC, (err, results) => {
      if (err) {
        console.log(err)
        response.status(400, err, res)
        return
      }
      console.log(results)
      res.json(results.rows)
    })
  })
}

const getMCIds = (id) => new Promise((resolve, reject) => {
  const sqlGetIdMCByRegionId = 'SELECT DISTINCT management_organization_id FROM ' + process.env.TABLE_FOR_HOUSES + " WHERE (region_id = '" + id + "' AND management_organization_id is not null);"
  console.log(sqlGetIdMCByRegionId)
  db.query(sqlGetIdMCByRegionId, (err, results, fields) => {
    if (err) {
      console.log(err)
    } else {
      resolve(results.rows)
    }
  })
})

const getOrganizations = (list) => new Promise((resolve, reject) => {
  const sql = `select * from ${process.env.TABLE_FOR_MC} where id in ${list};`
  console.time('sql_timer')
  db.query(sql, (err, result) => {
    if (err) {
      console.log(err)
    } else {
      console.timeEnd('sql_timer')
      resolve(result.rows)
    }
  })
})

const getMCinRegion = async (req, res) => {
  const regionId = req.params.id
  const organizationsIds = await getMCIds(regionId)
  const arrayIds = organizationsIds.map(elem => {
    return elem.management_organization_id
  })
  let stringListIds = '('
  for (const value of arrayIds) {
    stringListIds = stringListIds + `'${value}', `
  }
  stringListIds = stringListIds.slice(0, -2) + ')'
  const organizations = await getOrganizations(stringListIds)
  res.json(organizations)
  return true
}

module.exports = { getOrganizations, getHouses, getMCinRegion }
