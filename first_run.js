require('dotenv').config()
const lists = require('./lists')
const db = require('./dataBase/db')
const response = require('./support_server_files/response')

const createTable = (sql) => new Promise((resolve, reject) => {
  console.log('Создание таблицы')
  db.query(sql, (err, results) => {
    if (err) {
      console.log(err)
    } else {
      console.log('Таблица cоздана')
      resolve()
    }
  })
})

const createSQL = (listHeaders, table) => {
  console.log('Формирование запроса.')
  let sqlCreateTable = 'CREATE TABLE ' + table + '('
  for (const header of listHeaders) {
    sqlCreateTable = sqlCreateTable + ` ${header} text,`
  }
  sqlCreateTable = sqlCreateTable.slice(0, -1) + ');'
  console.log('Запрос сформирован.')
  return sqlCreateTable
}

const firstRun = async (req, res) => {
  let sqlCreateTable = createSQL(lists.headersHouses, process.env.TABLE_FOR_HOUSES)
  await createTable(sqlCreateTable)
  sqlCreateTable = createSQL(lists.headersMC, process.env.TABLE_FOR_MC)
  await createTable(sqlCreateTable)
  response.status(200, 'Подготовка закончена', res)
}

module.exports = { firstRun }
