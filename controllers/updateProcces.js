const fs = require('fs')
require('dotenv').config()
const path = require('path')
const unzipper = require('unzipper')
const download = require('download')
const db = require('../dataBase/db')
const lists = require('../lists')
const response = require('../support_server_files/response')
const zipName = 'zip_file.zip'

const dropData = (tableName) => new Promise((resolve, reject) => {
  console.log(`Начало процесса удаления данных из таблицы ${tableName}.`)
  const sqlDrop = `DELETE FROM ${tableName};`
  db.query(sqlDrop, (err) => {
    if (err) {
      console.log(err)
      return true
    }
    console.log(`Данные из таблицы ${tableName} успешно удалены.`)
    resolve()
  })
})

const removeFile = (fileName) => {
  console.log(`Файл ${fileName} готов к удалению.`)
  const filePath = path.join(__dirname, '..', 'download', fileName)
  if (filePath !== undefined) {
    try {
      fs.unlinkSync(filePath)
      console.log(`Файл ${fileName} удален`)
    } catch (e) {
      console.log(e)
    }
  }
}

const getFilename = (dir) => {
  const files = fs.readdirSync(dir).map(file => {
    console.log(`Файл в дирректории: ${dir}   ${file} >>>>>>>>>>>>>>>>>>>>>>>>>>>`)
    return file
  })
  return files[0]
}

const unzipFile = (zipName) => new Promise((resolve) => {
  console.log('Распаковка началась')
  fs.createReadStream(path.join(__dirname, '..', 'download', zipName))
    .pipe(unzipper.Extract({
      path: path.join(__dirname, '..', 'download')
    }))
    .on('close', () => {
      console.log(`Распаковка ${zipName} закончилась`)
      resolve()
    })
})

const dumpingCsv = (csvName, table, headers) => new Promise((resolve) => {
  let str = ''
  headers.forEach(elem => {
    str = str + elem + ', '
  })
  str = str.slice(0, -2)
  const sourceDir = path.join(__dirname, '..', 'download', csvName)
  const sqlDump = `COPY ${table}(${str}) FROM ` + "'" + `${sourceDir}` + '\'' + " DELIMITERS ';' CSV HEADER encoding 'utf-8';"
  // console.log(sqlDump)
  db.query(sqlDump, (error) => {
    if (error) {
      console.log(error)
    }
    console.log(csvName + '    Dumped')
    resolve()
    return true
  })
})
const downloadingData = async (url, fileName) => {
  try {
    console.log(`Началось скачивание по ссылке: ${url}.`)
    await download(url, path.join(__dirname, '..', 'download'), {
      filename: `${fileName}`
    })
    console.log(`Закончилось скачивание по ссылке: + ${url}.`)
    return true
  } catch (e) {
    console.log(e)
  }
}

const getData = async (zipName, link, table, headers) => {
  await downloadingData(link, zipName)
  await unzipFile(zipName)
  removeFile(zipName)
  const unZipFileName = getFilename(path.join(__dirname, '..', 'download'))
  console.log('unZipFileName:  ' + unZipFileName)
  await dumpingCsv(unZipFileName, table, headers)
  removeFile(unZipFileName)
}

const updateData = async (req, res) => {
  console.log('Начало процесса удаления данных.')
  dropData(process.env.TABLE_FOR_HOUSES)
  dropData(process.env.TABLE_FOR_MC)
  console.log('Процесс удаления данных закончен.')
  console.log('Начало процесса обновления данных о домах.')
  for (const index in lists.urlsHouses) {
    const linkHouses = lists.urlsHouses[index]
    const tableHouses = process.env.TABLE_FOR_HOUSES
    await getData(zipName, linkHouses, tableHouses, lists.headersHouses)
    console.log('Обновления данных о домах закончено.')
  }
  console.log('Начало процесса обновления об управляющих компаниях.')
  const linkMc = lists.urlManagmentCompanies
  await getData(zipName, linkMc, process.env.TABLE_FOR_MC, lists.headersMC)
  console.log('Конец процесса обновления об управляющих компаниях.')
  console.log('Конец процесса обновления БД.')
  response.status(200, 'OK', res)
}

module.exports = {
  updateData,
  downloadingData,
  dropData,
  removeFile,
  zipName
}
