const { firstRun } = require('../first_run')
const { getHouses, getMCinRegion } = require('../api')
const { updateData } = require('../controllers/updateProcces')

module.exports = (app) => {
  app.get('/first-run', firstRun)
  app.get('/update-data', updateData)
  app.get('/api/organization/:inn', getHouses)
  app.get('/api/organizations-in-region/:id', getMCinRegion)
}
