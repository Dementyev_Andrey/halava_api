const { downloadingData, zipName, dropData, removeFile } = require('../controllers/updateProcces')
const { getMCinRegion } = require('../api')
const config = require('../config')

describe('Control request to api about organizations in region', () => {
  getMCinRegion('/api/organizations-in-region/92b30014-4d52-4e2e-892d-928142b924bf').then(data => {
    expect(data).toBe(true)
  })
})

describe('Control updating process', () => {
  const testUrl = 'https://www.reformagkh.ru/opendata/export/118'

  test('Try drop old data', () => {
    dropData(config.TABLE_FOR_HOUSES).then(data => {
      expect(data).toBe(true)
    })
  })
  test('Try download document', () => {
    downloadingData(testUrl, zipName).then(data => {
      expect(data).toBe(true)
    })
  })
  test('Try remove file', () => {
    const result = removeFile('zip_file.zip')
    expect(result).toBe(undefined)
  })
})

