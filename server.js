const express = require('express')
const app = express()
require('dotenv').config()
const pool = require('./dataBase/db')

const PORT = process.env.PORT || 8000
const routes = require('./support_server_files/routes')
routes(app)

const runServer = async () => {
  try {
    pool.connect((error, client) => {
      if (error) {
        console.log(error)
      } else {
        console.log(`Connected to database ${process.env.POSTGRES_DB}`)
      }
    })
    app.listen(PORT, () => {
      console.log(`Server run on port ${PORT}`)
    })
  } catch (e) {
    console.log(e)
  }
}

runServer()
