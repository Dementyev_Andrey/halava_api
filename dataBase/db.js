const { Client } = require('pg')
require('dotenv').config()

const pool = new Client({
  host: process.env.HOST,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB
})

module.exports = pool
